CREATE DATABASE IF NOT EXISTS c9;
USE c9;

CREATE TABLE Projeto (
    id INT AUTO_INCREMENT,
    titulo VARCHAR(15) NOT NULL,
    texto VARCHAR(85) NOT NULL,
    imagem BLOB NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Curiosidades (
    id INT AUTO_INCREMENT,
    texto VARCHAR(214) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE TipoParticipante (
    id INT AUTO_INCREMENT,
    cargo VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Membros (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(45) NOT NULL,
    descricao VARCHAR(55) NOT NULL,
    imagem BLOB NOT NULL,
    FOREIGN KEY (TipoParticipante_id) REFERENCES TipoParticipante(id)
);


INSERT INTO TipoParticipante (cargo) VALUES ("Estudante");
INSERT INTO TipoParticipante (cargo) VALUES ("Monitor");
INSERT INTO TipoParticipante (cargo) VALUES ("Professor Formador");
INSERT INTO TipoParticipante (cargo) VALUES ("Coordenador");